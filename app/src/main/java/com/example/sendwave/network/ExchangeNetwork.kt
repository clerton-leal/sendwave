package com.example.sendwave.network

import com.example.sendwave.domain.Exchange
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface ExchangeNetwork {

    @GET("latest.json?app_id=5da601d19193455b9015e0a04ee569ca")
    fun getExchange(): Single<Exchange>

}