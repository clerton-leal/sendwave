package com.example.sendwave.injection

import com.example.sendwave.network.RetrofitProvider
import com.example.sendwave.network.ExchangeNetwork
import com.example.sendwave.viewmodel.SendMoneyViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

object Module {

    fun getModule() = module {
        single {
            RetrofitProvider.getRetrofit()
        }

        factory {
            val retrofit: Retrofit = get(Retrofit::class)
            retrofit.create(ExchangeNetwork::class.java)
        }

        viewModel {
            SendMoneyViewModel(get())
        }

    }

}