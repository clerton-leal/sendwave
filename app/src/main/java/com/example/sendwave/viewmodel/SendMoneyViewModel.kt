package com.example.sendwave.viewmodel

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sendwave.R
import com.example.sendwave.domain.Country
import com.example.sendwave.domain.SuccessSendMoney
import com.example.sendwave.network.ExchangeNetwork

class SendMoneyViewModel(private val exchangeNetwork: ExchangeNetwork): ViewModel() {

    private val flagIcon = MutableLiveData<Int>()
    private val errorPhoneNumber = MutableLiveData<Int>()
    private val errorAmount = MutableLiveData<Int>()
    private val convertedAmount = MutableLiveData<String>()
    private val errorSendMoney = MutableLiveData<Int>()
    private val successSendMoney = MutableLiveData<SuccessSendMoney>()

    private var phoneNumber: String? = null
    private var amountConvertedInBinary: String? = null
    private var initialAmountInBinary: String? = null

    fun validatePhoneNumber(inputPhoneNumber: String) {
        val phoneNumber = inputPhoneNumber.replace("+", "")
        this.phoneNumber = null
        when {
            !phoneNumberHasPrefix(phoneNumber) -> {
                fillUnknownFlagAndError(R.string.empty)
            }
            !phoneHasValidPrefix(phoneNumber) -> {
                fillUnknownFlagAndError(R.string.invalid_phone_code)
            }
            !phoneHasCorrectLength(phoneNumber) -> {
                fillFlagAndError(phoneNumber)
            }
            else -> {
                savePhoneNumberAndSetFlag(phoneNumber)
            }
        }
    }

    fun validateBinaryAmount(amount: String) {
        amountConvertedInBinary = null
        initialAmountInBinary = amount
        when {
            amount.isBlank() -> {
                errorAmount.postValue(R.string.empty)
                convertedAmount.postValue("")
            }
            amount.toCharArray().all { it == '0' || it == '1' } -> {
                errorAmount.postValue(R.string.empty)
                setConvertedAmount(amount)
            }
            else -> {
                errorAmount.postValue(R.string.only_binary_valid)
            }
        }
    }

    fun sendMoney(firstName: String, lastName: String) {
        val country = Country.fromPhoneNumber(phoneNumber)
        val phoneNumber = this.phoneNumber
        val amountConvertedInBinary = this.amountConvertedInBinary
        val initialAmountInBinary = this.initialAmountInBinary

        if (initialAmountInBinary != null
            && phoneNumber != null
            && amountConvertedInBinary != null
            && country != null
            && firstName.isNotBlank()
            && lastName.isNotBlank()) {
            successSendMoney.postValue(SuccessSendMoney(
                firstName,
                lastName,
                phoneNumber,
                initialAmountInBinary,
                country.displayName(),
                amountConvertedInBinary
            ))
        } else {
            errorSendMoney.postValue(R.string.fill_all_data)
        }
    }

    private fun phoneNumberHasPrefix(phoneNumber: String) = phoneNumber.length >= 3

    private fun savePhoneNumberAndSetFlag(phoneNumber: String) {
        this.phoneNumber = phoneNumber
        Country.fromPhoneNumber(phoneNumber)?.let {
            flagIcon.postValue(it.icon)
            errorPhoneNumber.postValue(R.string.empty)
        }
    }

    private fun fillUnknownFlagAndError(@StringRes errorMessage: Int) {
        flagIcon.postValue(R.drawable.unknow_flag)
        errorPhoneNumber.postValue(errorMessage)
    }

    private fun fillFlagAndError(phoneNumber: String) {
        val country = Country.fromPhoneNumber(phoneNumber)
        if (country != null) {
            flagIcon.postValue(country.icon)
        } else {
            flagIcon.postValue(R.drawable.unknow_flag)
        }

        errorPhoneNumber.postValue(R.string.invalid_phone_for_country)
    }

    private fun setConvertedAmount(amount: String) {
        convertedAmount.postValue("")
        val country = Country.fromPhoneNumber(phoneNumber)
        if (country == null) {
            errorAmount.postValue(R.string.valid_country_needed)
            return
        }

        exchangeNetwork.getExchange().subscribe({ exchange ->
            val countryRate = exchange.rates[country.abbreviation]
            if (countryRate != null) {
                amountConvertedInBinary = convertBinaryAmountToCountryRate(amount, countryRate)
                convertedAmount.postValue("Amount in the destination currency: $amountConvertedInBinary")
            } else {
                errorAmount.postValue(R.string.error_to_access_exchange)
            }
        }, {
            errorAmount.postValue(R.string.error_to_access_exchange)
        })
    }

    private fun convertBinaryAmountToCountryRate(amount: String, countryRate: Double): String {
        val amountInDecimal = amount.toInt(2)
        val convertedAmountInDecimal = amountInDecimal * countryRate
        return Integer.toBinaryString(convertedAmountInDecimal.toInt())
    }

    private fun phoneHasCorrectLength(phoneNumber: String): Boolean {
        val country = Country.fromPhoneNumber(phoneNumber)
        return if (country != null) {
            (phoneNumber.length - 3) == country.digitsAfterPrefix
        } else {
            true
        }
    }

    private fun phoneHasValidPrefix(phoneNumber: String) = Country.fromPhoneNumber(phoneNumber) != null

    fun flagIcon() = flagIcon as LiveData<Int>
    fun errorPhoneNumber() = errorPhoneNumber as LiveData<Int>
    fun errorAmount() = errorAmount as LiveData<Int>
    fun convertedAmount() = convertedAmount as LiveData<String>
    fun errorSendMoney() = errorSendMoney as LiveData<Int>
    fun successSendMoney() = successSendMoney as LiveData<SuccessSendMoney>

}