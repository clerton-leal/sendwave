package com.example.sendwave.domain

data class SuccessSendMoney(
    val firstName: String,
    val lastName: String,
    val phoneNumber: String,
    val binaryDollar: String,
    val country: String,
    val amountConvertedInBinary: String
)
