package com.example.sendwave.domain

import androidx.annotation.DrawableRes
import com.example.sendwave.R

enum class Country(
    val abbreviation: String,
    val phonePrefix: String,
    val digitsAfterPrefix: Int,
    @DrawableRes val icon: Int
) {
    KENYA("KES", "254", 9, R.drawable.kenya),
    NIGERIA("NGN", "234", 7, R.drawable.nigeria),
    TANZANIA("TZS", "255", 9, R.drawable.tanzania),
    UGANDA("UGX", "256", 7, R.drawable.uganda);

    fun displayName() = this.name.lowercase().replaceFirstChar { it.uppercase() }

    companion object {
        fun fromPhoneNumber(phoneNumber: String?) = values().find { it.phonePrefix == phoneNumber?.substring(0, 3) }
    }
}

