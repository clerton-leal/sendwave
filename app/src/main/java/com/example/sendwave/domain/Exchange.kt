package com.example.sendwave.domain

import kotlinx.serialization.Serializable

@Serializable
data class Exchange(
    val disclaimer: String,
    val license: String,
    val timestamp: Long,
    val base: String,
    val rates: Map<String, Double>
)
