package com.example.sendwave.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.sendwave.R
import com.example.sendwave.databinding.ActivitySendMoneyBinding
import com.example.sendwave.viewmodel.SendMoneyViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class SendMoneyActivity : AppCompatActivity() {

    private val viewModel by viewModel<SendMoneyViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySendMoneyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button.setOnClickListener {
            viewModel.sendMoney(
                binding.firstName.editText?.text.toString(),
                binding.lastName.editText?.text.toString()
            )
        }

        viewModel.flagIcon().observe(this) {
            binding.flag.setImageResource(it)
        }

        viewModel.errorPhoneNumber().observe(this) {
            binding.phoneNumber.error = getString(it)
        }

        viewModel.errorAmount().observe(this) {
            binding.amount.error = getString(it)
        }

        viewModel.convertedAmount().observe(this) {
            binding.convertedAmount.text = it
        }

        viewModel.errorSendMoney().observe(this) {
            binding.amount.error = getString(it)
        }

        viewModel.successSendMoney().observe(this) { successSend ->
            AlertDialog.Builder(this)
                .setMessage("""
                    To: ${successSend.firstName} ${successSend.lastName}
                    Phone: +${successSend.phoneNumber}
                    Sent: ${successSend.binaryDollar}
                    Country: ${successSend.country}
                    Amount in destination currency: ${successSend.amountConvertedInBinary}
                """)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }.create().show()
        }

        binding.phoneNumber.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                viewModel.validatePhoneNumber(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })

        binding.amount.editText?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                viewModel.validateBinaryAmount(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

}