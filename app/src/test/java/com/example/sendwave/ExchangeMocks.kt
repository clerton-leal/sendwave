package com.example.sendwave

import com.example.sendwave.domain.*

object ExchangeMocks {

    val exchangeWithoutRates = Exchange(
        "",
        "",
        0L,
        "",
        mapOf()
    )

    val exchangeWithKenyaRates = Exchange(
        "",
        "",
        0L,
        "",
        mapOf("KES" to 109.13737)
    )

    val successSendMoney = SuccessSendMoney(
        "Paul",
        "Anderson",
        "254123456789",
        "1111111",
        "Kenya",
        "11011000100100"
    )

}