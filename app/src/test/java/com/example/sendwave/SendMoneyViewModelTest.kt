package com.example.sendwave

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.sendwave.network.ExchangeNetwork
import com.example.sendwave.viewmodel.SendMoneyViewModel
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class SendMoneyViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: SendMoneyViewModel
    private lateinit var network: ExchangeNetwork

    @Before
    fun setUp() {
        network = mockk()
        viewModel = SendMoneyViewModel(network)
    }

    @Test
    fun `when phone number has less than3 characters should do not show error`() {
        viewModel.validatePhoneNumber("45")

        assertEquals(R.drawable.unknow_flag, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number has invalid prefix should show error`() {
        viewModel.validatePhoneNumber("456789")

        assertEquals(R.drawable.unknow_flag, viewModel.flagIcon().value)
        assertEquals(R.string.invalid_phone_code, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number is bellow of correct length for nigeria should show error`() {
        viewModel.validatePhoneNumber("234445567")

        assertEquals(R.drawable.nigeria, viewModel.flagIcon().value)
        assertEquals(R.string.invalid_phone_for_country, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number is bellow of correct length for kenya should show error`() {
        viewModel.validatePhoneNumber("25412345678")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.invalid_phone_for_country, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number is above of correct length for nigeria should show error`() {
        viewModel.validatePhoneNumber("23444556789")

        assertEquals(R.drawable.nigeria, viewModel.flagIcon().value)
        assertEquals(R.string.invalid_phone_for_country, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number is above of correct length for kenya should show error`() {
        viewModel.validatePhoneNumber("2541234567890")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.invalid_phone_for_country, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number correct length for nigeria should not show error`() {
        viewModel.validatePhoneNumber("2344455678")

        assertEquals(R.drawable.nigeria, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when phone number correct length for kenya should not show error`() {
        viewModel.validatePhoneNumber("254123456789")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertNull(viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when amount is not binary should show error`() {
        viewModel.validateBinaryAmount("254123456789")

        assertNull(viewModel.flagIcon().value)
        assertNull(viewModel.errorPhoneNumber().value)
        assertEquals(R.string.only_binary_valid, viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
    }

    @Test
    fun `when amount is binary but do not have a country should show error`() {
        viewModel.validateBinaryAmount("1111111")

        assertNull(viewModel.flagIcon().value)
        assertNull(viewModel.errorPhoneNumber().value)
        assertEquals(R.string.valid_country_needed, viewModel.errorAmount().value)
        assertEquals("", viewModel.convertedAmount().value)
    }

    @Test
    fun `when have error to get exchange should show error`() {
        every { network.getExchange() } returns Single.error(Throwable("Error"))

        viewModel.validatePhoneNumber("254123456789")
        viewModel.validateBinaryAmount("1111111")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertEquals(R.string.error_to_access_exchange, viewModel.errorAmount().value)
        assertEquals("", viewModel.convertedAmount().value)
    }

    @Test
    fun `when do not exists exchange for the country should show error`() {
        every { network.getExchange() } returns Single.just(ExchangeMocks.exchangeWithoutRates)

        viewModel.validatePhoneNumber("254123456789")
        viewModel.validateBinaryAmount("1111111")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertEquals(R.string.error_to_access_exchange, viewModel.errorAmount().value)
        assertEquals("", viewModel.convertedAmount().value)
    }

    @Test
    fun `when exists exchange for the country should show converted amount`() {
        every { network.getExchange() } returns Single.just(ExchangeMocks.exchangeWithKenyaRates)

        viewModel.validatePhoneNumber("254123456789")
        viewModel.validateBinaryAmount("1111111")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertEquals(R.string.empty, viewModel.errorAmount().value)
        assertEquals("Amount in the destination currency: 11011000100100", viewModel.convertedAmount().value)
    }

    @Test
    fun `when send money should show converted amount`() {
        every { network.getExchange() } returns Single.just(ExchangeMocks.exchangeWithKenyaRates)

        viewModel.validatePhoneNumber("254123456789")
        viewModel.validateBinaryAmount("1111111")
        viewModel.sendMoney("Paul", "Anderson")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertEquals(R.string.empty, viewModel.errorAmount().value)
        assertEquals("Amount in the destination currency: 11011000100100", viewModel.convertedAmount().value)
        assertNull(viewModel.errorSendMoney().value)
        assertEquals(ExchangeMocks.successSendMoney, viewModel.successSendMoney().value)
    }

    @Test
    fun `when send money without correct money should show error`() {
        every { network.getExchange() } returns Single.just(ExchangeMocks.exchangeWithKenyaRates)

        viewModel.validatePhoneNumber("254123456789")
        viewModel.validateBinaryAmount("1111119")
        viewModel.sendMoney("Paul", "Anderson")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.empty, viewModel.errorPhoneNumber().value)
        assertEquals(R.string.only_binary_valid, viewModel.errorAmount().value)
        assertNull(viewModel.convertedAmount().value)
        assertEquals(R.string.fill_all_data, viewModel.errorSendMoney().value)
        assertNull(viewModel.successSendMoney().value)
    }

    @Test
    fun `when send money without correct phone number should show error`() {
        every { network.getExchange() } returns Single.just(ExchangeMocks.exchangeWithKenyaRates)

        viewModel.validatePhoneNumber("2541234567890")
        viewModel.validateBinaryAmount("1111111")
        viewModel.sendMoney("Paul", "Anderson")

        assertEquals(R.drawable.kenya, viewModel.flagIcon().value)
        assertEquals(R.string.invalid_phone_for_country, viewModel.errorPhoneNumber().value)
        assertEquals(R.string.valid_country_needed, viewModel.errorAmount().value)
        assertEquals("", viewModel.convertedAmount().value)
        assertEquals(R.string.fill_all_data, viewModel.errorSendMoney().value)
        assertNull(viewModel.successSendMoney().value)
    }
}